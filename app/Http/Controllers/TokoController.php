<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\ProdukDetail;
use App\Models\Toko;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Webpatser\Uuid\Uuid;

class TokoController extends Controller
{    
    /**
     * Display Toko List
     *
     * @return View
     */
    public function tokoList() : View {
        $toko = Toko::all();

        return view('shofia.toko.toko-list', [
            'toko' => $toko
        ]);
    }

    /**
     * Display Tambah Toko Form
     *
     * @return View
     */
    public function tokoTambah() : View {
        return view('shofia.toko.toko-tambah', []);
    }
    
    /**
     * Display Edit Toko Form
     *
     * @param  mixed $request
     * @return View
     */
    public function tokoEdit(Request $request) : View | RedirectResponse {
        $toko = Toko::find($request->tokoId);

        if (empty($toko)) {
            return redirect()->route('toko-list');
        }

        return view('shofia.toko.toko-edit', [
            'toko' => $toko,
        ]);
    }
    
    /**
     * Display Produk in Toko
     *
     * @param  mixed $request
     * @return View
     */
    public function tokoProduk(Request $request) : View | RedirectResponse {
        $toko = Toko::find($request->tokoId);

        if (empty($toko)) {
            return redirect()->route('toko-list');
        }

        $tokoProduk = DB::table('tokos')
                        ->join('produk_details', 'tokos.id', '=', 'produk_details.produk_detail_toko_id')
                        ->join('produks', 'produk_details.produk_detail_produk_id', '=', 'produks.id')
                        ->select(
                            'produk_details.id AS detail_id',
                            'produks.produk_nama AS produk_nama',
                            'produks.produk_harga AS produk_harga',
                            'produk_details.produk_detail_stock AS produk_detail_stock',
                            'produk_details.produk_detail_status_jual AS produk_detail_status_jual'
                        )->where('tokos.id', $request->tokoId)
                        ->orderBy('produk_details.produk_detail_status_jual')->get();

        return view('shofia.toko.toko-produk', [
            'tokoProduk' => $tokoProduk,
            'toko' => $toko,
        ]);
    }
    
    /**
     * Display Form tambah produk to toko
     *
     * @param  mixed $request
     * @return View
     */
    public function tokoTambahProduk(Request $request) : View | RedirectResponse {
        $toko = Toko::find($request->tokoId);

        if (empty($toko)) {
            return redirect()->route('toko-list');
        }

        $produkInToko = ProdukDetail::select('produk_detail_produk_id')->where('produk_detail_toko_id', $request->tokoId)->get()->toArray();
        $produkInToko = array_column($produkInToko, 'produk_detail_produk_id');

        $produk = Produk::select('id' ,'produk_nama', 'produk_harga')
                            ->whereNotIn('id', $produkInToko)->get();

        return view('shofia.toko.toko-tambah-produk', [
            'toko' => $toko,
            'produk' => $produk,
        ]);
    }
    
    /**
     * Display Form edit produk di toko
     *
     * @param  mixed $request
     * @return View
     */
    public function tokoEditProduk(Request $request) : View | RedirectResponse {
        $produk = Produk::select(
                            'produk_nama',
                            'produk_details.id AS produk_detail_id',
                            'produk_details.produk_detail_stock',
                            'produk_details.produk_detail_status_jual'
                        )
                        ->join('produk_details', 'produks.id', '=', 'produk_details.produk_detail_produk_id')
                        ->where('produk_details.id', $request->produkId)->first();

        if (empty($produk)) {
            return redirect()->route('toko-list');
        }

        return view('shofia.toko.toko-edit-produk', [
            'produk' => $produk
        ]);
    }
    
    /**
     * insert new record on Toko models
     *
     * @param  mixed $request
     * @return RedirectResponse
     */
    public function doTokoTambah(Request $request) : RedirectResponse {
        $validatedData = $request->validate([
            'namaToko' => ['required', 'string', 'min:3'],
            'alamatToko' => ['required', 'string', 'min:3'],
        ]);

        try {
            Toko::create([
                'toko_nama' => $validatedData['namaToko'],
                'toko_alamat' => $validatedData['alamatToko'],
            ]);

            toastr()->addSuccess('Tambah toko berhasil.');
        } catch (\Throwable $th) {
            toastr()->addError('Tambah toko gagal.');
        }

        return Redirect::route('toko-list');
    }
    
    /**
     * proses edit Toko
     *
     * @param  mixed $request
     * @return RedirectResponse
     */
    public function doTokoEdit(Request $request) : RedirectResponse {
        $validatedData = $request->validate([
            'tokoId' => ['required', 'string'],
            'namaToko' => ['required', 'string', 'min:3'],
            'alamatToko' => ['required', 'string', 'min:3'],
        ]);

        try {
            $toko = Toko::find($validatedData['tokoId']);

            $toko->toko_nama = $validatedData['namaToko'];
            $toko->toko_alamat = $validatedData['alamatToko'];

            $toko->save();
            toastr()->addSuccess('Update toko berhasil.');
        } catch (\Throwable $th) {
            toastr()->addError('Update toko gagal.');
        }

        return Redirect::route('toko-list');
    }
    
    /**
     * proses tambah produk ke toko
     *
     * @param  mixed $request
     * @return RedirectResponse
     */
    public function doTambahProduk(Request $request) : RedirectResponse {
        $validatedData = $request->validate([
            'itemProduk' => ['required',],
            'tokoId' => ['required', 'string'],
            'stockProduk' => ['required'],
        ]);

        $produk = array();

        foreach ($validatedData['itemProduk'] as $key => $value) {
            array_push($produk, [
                'id' => Uuid::generate(4),
                'produk_detail_produk_id' => $value,
                'produk_detail_toko_id' => $validatedData['tokoId'],
                'produk_detail_stock' => $validatedData['stockProduk'],
                'created_at' => now()
            ]);
        }
        
        try {
            ProdukDetail::insert($produk);
            toastr()->addSuccess('Tambah produk berhasil.');
        } catch (\Throwable $th) {
            toastr()->addError('Tambah produk gagal.');
        }

        return redirect()->route('toko-produk-list', $validatedData['tokoId']);
    }
    
    /**
     * proses ubah status jual produk di toko
     *
     * @param  mixed $request
     * @return RedirectResponse
     */
    public function doUpdateStatusJualProduk(Request $request) : RedirectResponse {
        $validatedData = $request->validate([
            'detailId' => ['required', 'string'],
            'statusJual' => ['required', 'string', 'min:1', 'max:1'],
        ]);

        switch ($validatedData['statusJual']) {
            case 'Y':
                $produk_detail_status_jual = 'T';
                break;
            
            default:
                $produk_detail_status_jual = 'Y';
                break;
        }

        try {
            $detailProduk = ProdukDetail::find($validatedData['detailId']);

            $detailProduk->produk_detail_status_jual = $produk_detail_status_jual;

            $detailProduk->save();
            toastr()->addSuccess('Update status berhasil.');
        } catch (\Throwable $th) {
            toastr()->addError('Update status gagal.');
        }

        return redirect()->back();
    }
    
    /**
     * proses ubah data produk di toko
     *
     * @param  mixed $request
     * @return RedirectResponse
     */
    public function doEditProduk(Request $request) : RedirectResponse {
        $validatedData = $request->validate([
            'detailId' => ['required'],
            'stockToko' => ['required', 'integer', 'min_digits:1', 'max_digits:10000'],
            'statusJual' => ['required', 'string', 'min:1', 'max:1'],
        ]);

        $toko = Toko::select('tokos.id')
                    ->join('produk_details', 'tokos.id', '=', 'produk_details.produk_detail_toko_id')
                    ->where('produk_details.id', $validatedData['detailId'])->first();

        $produkDetail = ProdukDetail::find($validatedData['detailId']);

        if (empty($toko) || empty($produkDetail)) {
            return redirect()->route('toko-list');
        }

        try {
            $produkDetail->produk_detail_stock = $validatedData['stockToko'];
            $produkDetail->produk_detail_status_jual = $validatedData['statusJual'];
            $produkDetail->save();
            toastr()->addSuccess('Update produk berhasil.');
        } catch (\Throwable $th) {
            toastr()->addError('Update status gagal.');
        }

        return redirect()->route('toko-produk-list', $toko->id);
    }
}
