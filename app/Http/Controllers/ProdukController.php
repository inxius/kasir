<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class ProdukController extends Controller
{    
    /**
     * Display Produk List
     *
     * @return View
     */
    public function produkList() : View {
        $produk = Produk::all();

        return view('shofia.produk.produk-list', [
            'produk' => $produk
        ]);
    }
    
    /**
     * Display Tambah Produk Form
     *
     * @return View
     */
    public function produkTambah() : View {
        return view('shofia.produk.produk-tambah', []);
    }
    
    /**
     * Display Edit Produk Form
     *
     * @param  mixed $request
     * @return View
     */
    public function produkEdit(Request $request) : View | RedirectResponse {
        $produk = Produk::find($request->produkId);

        if (empty($produk)) {
            return redirect()->route('produk.produk-list');
        }

        $produk->produk_harga = intval($produk->produk_harga);

        return view('shofia.produk.produk-edit', [
            'produk' => $produk,
        ]);
    }
    
    /**
     * insert new record on Produk models
     *
     * @param  mixed $request
     * @return RedirectResponse
     */
    public function doTambahProduk(Request $request) : RedirectResponse {
        $validatedData = $request->validate([
            'namaProduk' => ['required', 'string', 'min:3'],
            'hargaProduk' => ['required', 'integer', 'min_digits:3', 'max_digits:6'],
        ]);

        try {
            Produk::create([
                'produk_nama' => $validatedData['namaProduk'],
                'produk_harga' => $validatedData['hargaProduk'],
            ]);

            toastr()->addSuccess('Tambah produk berhasil.');
        } catch (\Throwable $th) {
            toastr()->addError('Tambah produk gagal.');
        }

        return Redirect::route('produk.produk-list');
    }
    
    /**
     * proses edit Produk
     *
     * @param  mixed $request
     * @return RedirectResponse
     */
    public function doEditProduk(Request $request) : RedirectResponse {
        $validatedData = $request->validate([
            'produkId' => ['required'],
            'namaProduk' => ['required', 'string', 'min:3'],
            'hargaProduk' => ['required', 'integer', 'min_digits:3', 'max_digits:6'],
        ]);

        try {
            $produk = Produk::find($validatedData['produkId']);
            $produk->produk_nama = $validatedData['namaProduk'];
            $produk->produk_harga = $validatedData['hargaProduk'];
            $produk->save();
            toastr()->addSuccess('Edit produk berhasil.');
        } catch (\Throwable $th) {
            toastr()->addError('Edit produk gagal.');
        }

        return Redirect::route('produk.produk-list');
    }
}
