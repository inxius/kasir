<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('produk_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('produk_detail_produk_id')
                ->constrained(
                    table: 'produks', indexName: 'produk_details_produks_id'
                );
            $table->foreignId('produk_detail_toko_id')
                ->constrained(
                    table: 'tokos', indexName: 'produk_details_tokos_id'
                );
            $table->integer('produk_detail_stock');
            $table->enum('produk_detail_status_jual', ['Y', 'T'])->default('Y');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('produk_details');
    }
};
