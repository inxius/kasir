<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transaksi_details', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('transaksi_detail_transaksi_id');
            $table->foreign('transaksi_detail_transaksi_id')->references('id')->on('transaksis');
            $table->foreignId('transaksi_detail_produk_id')
                ->constrained(
                    table: 'produks', indexName: 'transaksi_details_produks_id'
                );
            $table->integer('transaksi_detail_jumlah');
            $table->decimal('transaksi_detail_harga', $precision = 18, $scale = 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaksi_details');
    }
};
