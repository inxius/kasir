<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignId('transaksi_user_id')
                ->constrained(
                    table: 'users', indexName: 'transaksis_users_id'
                );
            $table->foreignId('transaksi_toko_id')
                ->constrained(
                    table: 'tokos', indexName: 'transaksis_tokos_id'
                );
            $table->decimal('transaksi_bayar', $precision = 18, $scale = 2);
            $table->date('transaksi_tanggal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaksis');
    }
};
