<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'kasir']);
        Role::create(['name' => 'guest']);

        $userAdmin = \App\Models\User::factory()->create([
            'email' => 'admin@admin.com',
            'user_level' => 'admin',
        ]);
        $userAdmin->assignRole('admin');

        for ($i=0; $i < 3; $i++) { 
            $userGuest = \App\Models\User::factory()->create();
            $userGuest->assignRole('guest');
        }

        \App\Models\Toko::factory(2)->create();

        \App\Models\Produk::factory(10)->create();

        $this->call([
            ProdukDetailSeeder::class,
        ]);
    }
}
