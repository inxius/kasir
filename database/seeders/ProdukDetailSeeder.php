<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Toko;
use App\Models\Produk;

class ProdukDetailSeeder extends Seeder
{
    protected $produk;
    protected $toko;

    function __construct()
    {
        $this->produk = Produk::all();
        $this->toko = Toko::all();
    }

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->toko as $toko) {
            foreach ($this->produk as $produk) {
                \App\Models\ProdukDetail::factory()->create([
                    'produk_detail_produk_id' => $produk->id,
                    'produk_detail_toko_id' => $toko->id
                ]);
            }
        }
    }
}
