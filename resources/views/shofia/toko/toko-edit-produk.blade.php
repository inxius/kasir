<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      {{ __('Toko') }}
    </h2>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
        <div class="p-6 text-gray-900">
          <div class="max-w-xl">

            <header class="py-4">
              <h2 class="text-lg font-medium text-gray-900">
                  {{ __('Edit ' . $produk->produk_nama) }}
              </h2>
            </header>

            <form method="POST" action="{{ route('toko.produk.do-edit-produk') }}">
              @csrf
              @method('PUT')
              <input type="hidden" name="detailId" value={{ $produk->produk_detail_id }}>
              <div class="flex flex-col gap-4">
                <div>
                  <x-input-label for="stockToko" :value="__('Stock produk di toko')" />
                  <x-text-input id="stockToko" name="stockToko" type="number" min=1 max=10000 value="{{ $produk->produk_detail_stock }}" class="mt-1 block w-full" required autofocus />
                  <x-input-error class="mt-2" :messages="$errors->get('stockToko')" />
                </div>

                <div>
                  <x-input-label for="statusJualToko" :value="__('Status jual produk di toko')" />
                  <fieldset id="statusJualToko" class="inline-flex gap-4">
                    <div>
                      <input type="radio" name="statusJual" id="radioYes" value="Y" {{ $produk->produk_detail_status_jual == 'Y' ? 'checked' : '' }}>
                      <label for="radioYes">Dijual</label>
                    </div>
                    <div>
                      <input type="radio" name="statusJual" id="radioNo" value="T" {{ $produk->produk_detail_status_jual == 'T' ? 'checked' : '' }}>
                      <label for="radioNo">Tidak Dijual</label>
                    </div>
                  </fieldset>
                </div>

                <div class="flex items-center gap-4">
                  <x-primary-button>{{ __('Simpan') }}</x-primary-button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</x-app-layout>
