<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      {{ __('Toko') }}
    </h2>
  </x-slot>

  <x-slot name="customJavascript">
    <script>
      let table = new DataTable('#myTable', {
        order: false
      });
    </script>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
        <div class="p-6 text-gray-900">
          <div class="inline-flex py-5 gap-4 items-center">
            <a href="{{ route('toko-tambah') }}">
              <button 
                type="button" 
                class="text-white bg-blue-700 
                      hover:bg-blue-800 focus:ring-4 
                      focus:ring-blue-300 font-medium 
                      rounded-lg text-sm px-5 py-2.5 me-2 mb-2 
                      dark:bg-blue-600 dark:hover:bg-blue-700 
                      focus:outline-none dark:focus:ring-blue-800"
              >
              <div class="flex justify-center items-center gap-2">
                <span class="material-symbols-outlined">
                  add_circle
                </span> 
                Tambah
              </div>
              </button>
            </a>
          </div>
          
          <div>
            <table id="myTable" class="display">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Toko</th>
                  <th>Alamat Toko</th>
                  <th>Aksi</th>
                </tr>
              </thead>
                <tbody>
                  @foreach ($toko as $key => $item)
                    <tr>
                      <td>{{ $key + 1 }}</td>
                      <td>{{ $item->toko_nama }}</td>
                      <td>{{ $item->toko_alamat }}</td>
                      <td>
                        <div class="inline-flex gap-2">
                          <a href={{ route('toko-produk-list', $item->id) }}>
                            <button type="button" title="Show Produk">
                              <span class="material-symbols-outlined">
                                package_2
                              </span>
                            </button>
                          </a>

                          <a href={{ route('toko-edit', $item->id) }}>
                            <button type="button" title="Edit Toko">
                              <span class="material-symbols-outlined">
                                edit
                              </span>
                            </button>
                          </a>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</x-app-layout>
