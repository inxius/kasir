<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      {{ __($toko->toko_nama ?  $toko->toko_nama : 'Toko') }}
    </h2>
  </x-slot>

  <x-slot name="customJavascript">
    <script>
      let table = new DataTable('#myTable', {
        order: false
      });
    </script>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
        <div class="p-6 text-gray-900">

          <form method="POST" action="{{ route('toko-do-tambah-produk') }}" class="flex flex-col">
            @csrf
            <div class="inline-flex py-5 gap-4 items-center">
              <button 
                type="submit"
                class="text-white bg-blue-700 
                      hover:bg-blue-800 focus:ring-4 
                      focus:ring-blue-300 font-medium 
                      rounded-lg text-sm px-5 py-2.5 me-2 mb-2 
                      dark:bg-blue-600 dark:hover:bg-blue-700 
                      focus:outline-none dark:focus:ring-blue-800"
              >
                <i class="fa-solid fa-plus me-2"></i> Tambah Produk
              </button>
            </div>

            <div>
              <input type="hidden" name="tokoId" value={{ $toko->id }}>
              
              <div class="max-w-sm pb-4">
                <x-input-label for="stockProduk" :value="__('Stock Produk')" />
                <x-text-input id="stockProduk" name="stockProduk" type="number" min="1" max="10000" class="mt-1 block w-full" required autofocus />
                <x-input-error class="mt-2" :messages="$errors->get('stockProduk')" />
              </div>
            </div>

            <div>
              <table id="myTable" class="display">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Aksi</th>
                    <th>Nama Produk</th>
                    <th>Harga</th>
                  </tr>
                </thead>
                  <tbody>
                    @php
                      $no = 0;
                    @endphp
                    <fieldset>
                      @foreach ($produk as $item)
                        @if ($item->produk_detail_status_jual != 'Y' && $item->produk_detail_status_jual != 'T')
                          <tr>
                            <td>{{ ++$no }}</td>
                            <td>
                              <label>
                                <input type="checkbox" name="itemProduk[]" id="" value={{ $item->id }}>
                              </label>
                            </td>
                            <td>{{ $item->produk_nama }}</td>
                            <td>Rp. {{ $item->produk_harga }}</td>
                          </tr>
                        @endif
                      @endforeach
                    </fieldset>
                  </tbody>
              </table>
            </div>
          </form>
          
        </div>
      </div>
    </div>
  </div>
</x-app-layout>
