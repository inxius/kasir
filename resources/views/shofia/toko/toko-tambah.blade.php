<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      {{ __('Toko') }}
    </h2>
  </x-slot>

  <x-slot name="customJavascript">
    <script>
      let table = new DataTable('#myTable', {
        order: false
      });
    </script>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
        <div class="p-6 text-gray-900">
          <div class="max-w-xl">

            <header class="py-4">
              <h2 class="text-lg font-medium text-gray-900">
                  {{ __('Tambah Toko') }}
              </h2>
            </header>

            <form method="POST" action="{{ route('do-toko-tambah') }}">
              @csrf

              <div class="flex flex-col gap-4">
                <div>
                  <x-input-label for="namaToko" :value="__('Nama Toko')" />
                  <x-text-input id="namaToko" name="namaToko" type="text" class="mt-1 block w-full" required autofocus />
                  <x-input-error class="mt-2" :messages="$errors->get('namaToko')" />
                </div>
  
                <div>
                  <x-input-label for="alamatToko" :value="__('Alamat Toko')" />
                  <x-text-area id="alamatToko" name="alamatToko" type="text" class="mt-1 block w-full" required autofocus />
                  <x-input-error class="mt-2" :messages="$errors->get('alamatToko')" />
                </div>

                <div class="flex items-center gap-4">
                  <x-primary-button>{{ __('Simpan') }}</x-primary-button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</x-app-layout>
