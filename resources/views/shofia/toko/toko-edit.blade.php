<x-app-layout>
    <x-slot name="header">
      <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Toko') }}
      </h2>
    </x-slot>
  
    <div class="py-12">
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
          <div class="p-6 text-gray-900">
            <div class="max-w-xl">
  
              <header class="py-4">
                <h2 class="text-lg font-medium text-gray-900">
                    {{ __('Edit Toko') }}
                </h2>
              </header>
  
              <form method="POST" action="{{ route('do-toko-edit') }}">
                @csrf
                @method('PUT')
                <input type="hidden" name="tokoId" value={{ $toko->id }}>
                <div class="flex flex-col gap-4">
                  <div>
                    <x-input-label for="namaToko" :value="__('Nama Toko')" />
                    <x-text-input id="namaToko" name="namaToko" type="text" value="{{ $toko->toko_nama }}" class="mt-1 block w-full" required autofocus />
                    <x-input-error class="mt-2" :messages="$errors->get('namaToko')" />
                  </div>
    
                  <div>
                    <x-input-label for="alamatToko" :value="__('Alamat Toko')" />
                    <x-text-area id="alamatToko" name="alamatToko" class="mt-1 block w-full" required>
                      {{ $toko->toko_alamat }}
                    </x-text-area>
                    <x-input-error class="mt-2" :messages="$errors->get('alamatToko')" />
                  </div>
  
                  <div class="flex items-center gap-4">
                    <x-primary-button>{{ __('Simpan') }}</x-primary-button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </x-app-layout>
  